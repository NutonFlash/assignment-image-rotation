#ifndef PADDING_H
#define PADDING_H

#include "image.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

uint32_t calculate_padding(uint32_t image_width);

uint32_t padding_size_bytes(struct image const*img);

bool file_write_padding(FILE* const out, uint32_t image_width);

#endif
