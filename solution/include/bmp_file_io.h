#ifndef BMP_FILE_IO_H
#define BMP_FILE_IO_H
#include "image.h"

#include <stdio.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_NULL_PTR,
    ALLOCATE_DATA_ERROR
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_PADDING_ERROR,
    WRITE_NULL_PTR
};

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status to_bmp(FILE* out, struct image const* img);


#endif
