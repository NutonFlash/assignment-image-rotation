#include "../include/padding.h"

uint32_t calculate_padding(uint32_t image_width) {
    return (4 - ((image_width * sizeof (struct pixel)) % 4)) % 4;
}

uint32_t padding_size_bytes(struct image const *img) {
    return img->width * img->height * calculate_padding(img->width);
}

bool file_write_padding(FILE* const out, uint32_t image_width) {
    size_t zero = 0;
    if(fwrite(&zero, calculate_padding(image_width), 1, out) == 0 && calculate_padding(image_width) != 0)
        return false;
    return true;
}

