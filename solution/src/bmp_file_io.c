#include "../include/bmp_file_io.h"
#include "../include/bmp_header.h"
#include "../include/padding.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>


static enum read_status read_pixels (FILE* in, struct bmp_header const* header, struct image* img) {
    struct pixel pixel = {0};
    if(!header_size_valid(header))
        return false;
    *img = image_create(header->biWidth, header->biHeight);
    if (!img->data)
        return ALLOCATE_DATA_ERROR;
    fseek(in, header->bOffBits, SEEK_SET);
    for (uint32_t i = 0; i < img->height; i++) {
        for (uint32_t j = 0; j < img->width; j++) {
            if (!fread(&pixel, sizeof (struct pixel), 1, in)) {
                image_destroy(img);
                return READ_INVALID_BITS;
            }
            image_set_pixel(img, pixel, j, i);
        }
        fseek(in, calculate_padding(img->width), SEEK_CUR);
    }
    return READ_OK;
}

static enum write_status write_pixels (FILE* out, struct image const* img) {
    struct pixel pixel;
    for (uint32_t i = 0; i < img->height; i++){
        for (uint32_t j = 0; j < img->width; j++) {
            pixel = image_get_pixel(img, j, i);
            if (!fwrite(&pixel, sizeof (struct pixel), 1, out))
                return WRITE_ERROR;
        }
        if(!file_write_padding(out, img->width))
            return WRITE_PADDING_ERROR;
    }
    return WRITE_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};
    if(in == NULL || img == NULL)
        return READ_NULL_PTR;
    if (!fread(&header, sizeof(struct bmp_header), 1, in))
        return READ_INVALID_HEADER;
    if (!header_signature_valid(&header))
        return READ_INVALID_SIGNATURE;
    if (!header_size_valid(&header))
        return READ_INVALID_HEADER;
    return read_pixels(in, &header, img);
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header;
    if(out == NULL || img == NULL)
        return WRITE_NULL_PTR;
    header = header_from_image(img);
    if (!fwrite(&header, sizeof (struct bmp_header), 1, out))
        return WRITE_ERROR;
    return write_pixels(out, img);
}
