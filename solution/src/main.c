#include "../include/transformer.h"
#include "../include/bmp_file_io.h"
#include "../include/error_printer.h"
#include "../include/file_manager.h"

#include <stdio.h>


static const unsigned read_error_codes[] = {
        [READ_INVALID_SIGNATURE] = ERROR_INVALID_SIGNATURE,
        [READ_INVALID_HEADER] = ERROR_INVALID_HEADER,
        [READ_INVALID_BITS] = ERROR_INVALID_BITS,
        [READ_NULL_PTR] = ERROR_NULL_PTR,
        [ALLOCATE_DATA_ERROR] = ERROR_MEMORY_ALLOCATION
};

int main(int argc, char **argv) {
    FILE* in = NULL;
    FILE* out = NULL;
    struct image img = {0};
    struct image rotated;
    enum read_status read_status;

    if (argc != 3) {
        print_error(ERROR_ARGS);
        return 1;
    }
    if (!(open_file(&in, argv[1], "rb") && open_file(&out, argv[2], "wb"))) {
        print_error(ERROR_OPEN);
        return 1;
    }
    read_status = from_bmp(in, &img);
    if (read_status != READ_OK) {
        print_error(read_error_codes[read_status]);
        image_destroy(&img);
        close_file(in);
        close_file(out);
        return 1;
    }
    rotated = rotate(img);
    image_destroy(&img);
    if (to_bmp(out, &rotated)) {
        print_error(ERROR_WRITE);
        image_destroy(&rotated);
        close_file(in);
        close_file(out);
        return 1;
    }
    image_destroy(&rotated);
    if (!(close_file(in) && close_file(out))) {
        print_error(ERROR_CLOSE);
        return 1;
    }
    return 0;
}
