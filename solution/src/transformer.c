#include "../include/transformer.h"
#include "image.h"

#include <stdint.h>
#include <stdio.h>

#define INVALID_CORD 0

struct image rotate(struct image const source) {
    if(source.data == NULL)
        return image_create(INVALID_CORD, INVALID_CORD);
    struct image rotated = image_create(source.height, source.width);
    for(uint32_t i = 0; i < source.height; i++)
        for(uint32_t j = 0; j < source.width; j++) {
            image_set_pixel(&rotated, image_get_pixel(&source, j, source.height - i - 1), i, j);
        }
    return rotated;
}
