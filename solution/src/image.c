#include "../include/image.h"

#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>


struct image image_create (uint32_t width, uint32_t height) {
    struct image img = {0};
    img.width = width;
    img.height = height;
    img.data = malloc (width * height * sizeof (struct pixel));
    if (!img.data) {
        img.width = 0;
        img.height = 0;
    }
    return img;
}

static bool image_check_coords(struct image const* img, uint32_t x, uint32_t y) {
    return x >= 0 && x <= img->width && y >= 0 && y <= img->height;
}

bool image_cords_valid(struct image const* img) {
    return image_check_coords(img, img->width, img->height);
}

bool image_set_pixel(struct image *img, struct pixel const p, uint32_t x, uint32_t y) {
    if(!image_check_coords(img, x, y))
        return false;
    *(img->data + y * img->width + x) = p;
    return true;
}

uint32_t image_get_size_bytes(struct image const* img) {
    return img->width * img->height * sizeof(struct pixel);
}

struct pixel image_get_pixel(struct image const* img, uint32_t x, uint32_t y) {
    return *(img->data + y * img->width + x);
}

void image_destroy(struct image *img) {
    free(img->data);
}

